// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2018, 2020, 2022 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use anyhow::Result;
use regex::Regex;
use reqwest::Client;
use rocket::State;
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashMap};
use std::path::PathBuf;
use std::sync::OnceLock;

const REPOSITORY_BASE: &str = "https://apt.wikimedia.org/wikimedia";
const USER_AGENT: &str = toolforge::user_agent!("apt-browser");
static DIST_RE: OnceLock<Regex> = OnceLock::new();

async fn fetch_dists(client: &Client) -> Result<Vec<String>> {
    let req = client.get(format!("{}/dists/", REPOSITORY_BASE)).build()?;
    let resp = client.execute(req).await?;
    let text = resp.error_for_status()?.text().await?;
    let regex = DIST_RE.get_or_init(|| {
        Regex::new(r#"<a href="([a-z\-]*?)/">([a-z\-]*?)/</a>"#).unwrap()
    });
    let mut dists = vec![];
    for cap in regex.captures_iter(&text) {
        dists.push(cap[1].to_string());
    }
    Ok(dists)
}

async fn fetch_components(client: &Client, dist: &str) -> Result<Vec<String>> {
    let req = client
        .get(format!("{}/dists/{dist}/Release", REPOSITORY_BASE))
        .build()?;
    let resp = client.execute(req).await?;
    let text = resp.error_for_status()?.text().await?;
    for line in text.split('\n') {
        if let Some(list) = line.strip_prefix("Components: ") {
            let components =
                list.split(' ').map(|val| val.to_string()).collect();
            return Ok(components);
        }
    }

    panic!("Invalid {dist} Release file")
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Package {
    package: String,
    version: String,
}

async fn fetch_packages(
    client: &Client,
    dist: &str,
    component: &str,
) -> Result<BTreeMap<String, String>> {
    let req = client
        .get(format!(
            "{}/dists/{dist}/{component}/binary-amd64/Packages",
            REPOSITORY_BASE
        ))
        .build()?;
    let resp = client.execute(req).await?;
    let text = resp.error_for_status()?.text().await?;
    let packages: Vec<Package> = rfc822_like::from_str(&text)?;
    Ok(packages
        .into_iter()
        .map(|package| (package.package, package.version))
        .collect())
}

#[derive(Serialize)]
struct IndexTemplate {
    dists: Vec<String>,
    components: HashMap<String, Vec<String>>,
}

#[get("/")]
async fn index(client: &State<Client>) -> Template {
    let dists = fetch_dists(client).await.unwrap();
    let mut components = HashMap::new();
    for dist in &dists {
        components.insert(
            dist.to_string(),
            fetch_components(client, dist).await.unwrap(),
        );
    }
    Template::render("index", IndexTemplate { dists, components })
}

#[derive(Serialize)]
struct PackagesTemplate {
    dist: String,
    comp: String,
    packages: BTreeMap<String, String>,
}

#[derive(Responder)]
#[response(status = 500)]
struct BadInput(&'static str);

#[get("/<dist>/<component..>")]
async fn packages(
    dist: &str,
    component: PathBuf,
    client: &State<Client>,
) -> Result<Template, BadInput> {
    let comp = match component.to_str() {
        Some(comp) => comp.to_string(),
        None => {
            return Err(BadInput("invalid component"));
        }
    };
    if !fetch_dists(client)
        .await
        .unwrap()
        .contains(&dist.to_string())
    {
        return Err(BadInput("invalid dist"));
    }
    if !fetch_components(client, dist)
        .await
        .unwrap()
        .contains(&comp)
    {
        return Err(BadInput("invalid component"));
    }
    let packages = fetch_packages(client, dist, &comp).await.unwrap();
    Ok(Template::render(
        "packages",
        PackagesTemplate {
            dist: dist.to_string(),
            comp,
            packages,
        },
    ))
}

#[launch]
fn rocket() -> _ {
    let client = Client::builder().user_agent(USER_AGENT).build().unwrap();
    rocket::build()
        .mount("/", routes![index, packages])
        .manage(client)
        .attach(Template::fairing())
        .attach(Healthz::fairing())
}

#[cfg(test)]
mod test {
    use super::rocket;
    use rocket::http::Status;
    use rocket::local::blocking::Client;

    #[test]
    fn test_index() {
        let client = Client::tracked(rocket()).unwrap();
        let response = client.get("/").dispatch();
        assert_eq!(response.status(), Status::Ok);
        let response = response.into_string().unwrap();
        assert!(response.contains("buster-wikimedia"));
        assert!(response.contains("thirdparty&#x2F;hwraid"));
    }

    #[test]
    fn test_package() {
        let client = Client::tracked(rocket()).unwrap();
        let response = client.get("/buster-wikimedia/main/").dispatch();
        assert_eq!(response.status(), Status::Ok);
        let response = response.into_string().unwrap();
        assert!(response.contains("acme-chief"));
    }
}
